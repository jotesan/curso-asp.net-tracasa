﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio02_Herencia
{
    public class Empleado : Ejercicio01_Encapsulacion.Usuario
    {
        float salario;
        Coche suCoche;

        public Empleado()
        {
        }

        public Empleado(string nombre, int edad, float altura, float salario) : base(nombre, edad, altura)
        {
            Salario = salario;
        }

        public override string ToString()
        {
            return base.ToString() + "  Salario: " + salario + " euros " + (SuCoche == null ? "" : "Posee coche: " + SuCoche);
        }

        public float Salario
        {
            get
            {
                return salario;
            }
            set
            {
                salario = (value < 7000) ? 7000 : value;
            }
        }

        public Coche SuCoche { get => suCoche; set => suCoche = value; }

        public override void PedirPorConsola()
        {
            base.PedirPorConsola();
            Utilidades.PedirNumero<float>("Salario", out salario);
        }
    }
}
