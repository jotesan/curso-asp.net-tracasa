﻿using System;
using Ejercicio01_Encapsulacion;

namespace Ejercicio02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario[] usuarios = new Usuario[4];

            usuarios[0] = new Usuario("Luke", 20, 1.70f);
            usuarios[1] = new Usuario("Leia", 20, 1.75f);
            usuarios[2] = new Empleado("Han", 35, 1.80f, 10500.70f);
            usuarios[3] = new Empleado("Chew", 40, 2.10f, 6000.50f);

            for (int i = 0; i < usuarios.Length; i++)
            {
                if (usuarios[i].GetType().Equals(typeof(Empleado)))
                {
                    ((Empleado)usuarios[i]).Salario += 1000;
                }
                Console.WriteLine("Usuario " + (i + 1) + ": " + usuarios[i].ToString());
            }
        }
    }
}
