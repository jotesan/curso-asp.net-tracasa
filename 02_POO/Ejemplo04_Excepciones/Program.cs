﻿using System;

namespace Ejemplo04_Excepciones
{
    class Program
    {
        static void FuncionRecursivaInfinita(int i)
        {
            if (i < 100) FuncionRecursivaInfinita(i++);          
        }
        static void Main(string[] args)
        {
            try
            {
                FuncionRecursivaInfinita(200);
            }
            catch (StackOverflowException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                Console.WriteLine("Conviertiendo lo inconvertible!");
                int.Parse("No NUMERO");
                Console.WriteLine("Esta linea NI SE MUESTRA");
            }
            catch (FormatException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                Console.WriteLine("Conviertiendo lo inconvertible!");
                int.Parse("No NUMERO");
                Console.WriteLine("Esta linea NI SE MUESTRA");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYMenos = null;
                Console.WriteLine(nadaYMenos.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYMenos = null;
                Console.WriteLine(nadaYMenos.ToString());
            }
            catch (FormatException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                throw new FormatException("Error de formato porque me da la gana");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            try
            {
                FuncionQueDelegaExcepcion();
            }
            catch (FormatException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }            
        }
        
        public static void FuncionQueDelegaExcepcion()
        {
            int.Parse("ERROR");
        }
    }
}
