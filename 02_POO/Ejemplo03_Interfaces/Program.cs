﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;

namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico("Fiat", "Punto", 9000, 100);
            popurri[2] = new Usuario("Fulanito", 50, 2);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche)popurri[0];
            Console.WriteLine(fiatPunto.GetNombre());
            INombrable fiatNombrable = fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre);
            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.Nombre);
            EjemploLista();
        }

        static void EjemploLista()
        {
            List<string> textos = new List<string>(3);
            textos.Add("Primer texto");
            textos.Add("2do texto");
            textos.Add("Tercer texto");
            textos.Add("Otro texto");

            for (int i = 0; i < 5; i++)
            {
                textos.Add("Texto " + i + " º ");
            }

            textos.RemoveAt(2);
            textos.Remove("Otro texto");

            foreach (string texto in textos)
            {
                Console.WriteLine("Elem: " + texto);
            }

            List<Coche> coches = new List<Coche>();
            coches.Add(new Coche("Toyota", "Cupra", 50000));
            coches.Add(new CocheElectrico("Tesla", "F21", 40000));
            coches[0].MostrarEnConsola();

            IList<Coche> icoches = coches;
            icoches.Add(new Coche());
            IList<Usuario> listaUsers = new List<Usuario>();
            listaUsers.Add(new Usuario("Usuario de Lista 1", 0, 0));
            listaUsers.Add(new Usuario("Usuario de Lista 2", 0, 0));

            IList<Usuario> arrayUsers = new Usuario[10];
            arrayUsers[0] = new Usuario("Usuario de Lista 2", 0, 0);
            arrayUsers[1] = new Usuario("Usuario de Lista 2", 0, 0);
            arrayUsers[5] = new Usuario("Usuario de Lista 2", 0, 0);

            MostrarColeccion(listaUsers);
            MostrarColeccion(arrayUsers);
        }

        static void MostrarColeccion(ICollection<Usuario> icoleccion)
        {
            Console.WriteLine("Coleccion de usuarios " + icoleccion.GetType());
            foreach (Usuario user in icoleccion)
            {
                if (user != null) user.MostrarEnConsola();
            }
        }
    }
}
