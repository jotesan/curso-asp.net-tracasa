﻿using System;
using Ejemplo01_Encapsulacion;

namespace Ejemplo02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche("Seat", "Ibiza", 12000);
            Coche miCoche2 = new Coche("Seat", "Cordoba", 12000);
            miCoche.Acelerar();

            Console.WriteLine("To String 1: " + miCoche.ToString());
            Console.WriteLine("Hash code 1: " + miCoche.GetHashCode());
            Console.WriteLine("Hash code 2: " + miCoche2.GetHashCode());
            Console.WriteLine("Type: " + miCoche2.GetType().ToString());
            if (miCoche.GetType().Equals(typeof(Coche)))
            {
                Console.WriteLine("miCoche es un coche! ");
            }
            miCoche2.Modelo = "Ibiza";
            if (miCoche.Equals(miCoche2))
            {
                Console.WriteLine("miCoche es igual al 2! ");
            }
            else
            {
                Console.WriteLine("miCoche es diferente al 2! ");
            }

            CocheElectrico miTesla = new CocheElectrico();
            miTesla.Modelo = "SX";
            miTesla.Marca = "Tesla";
            Console.WriteLine(miTesla.ToString());
            miTesla.Acelerar();
            miTesla.Acelerar();
            Console.WriteLine("Velocidad: " + miTesla.Velocidad + "\nNivel de bateria: " + miTesla.NivelBateria + "%. ");

            Coche miTeslaComoCoche = miTesla;
            
            Console.WriteLine(miTeslaComoCoche.ToString());
            miTeslaComoCoche.Acelerar();
            Console.WriteLine("Velocidad: " + miTeslaComoCoche.Velocidad);
            Console.WriteLine(miTeslaComoCoche.ToString());
        }
    }
}
