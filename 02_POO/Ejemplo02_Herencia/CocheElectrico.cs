﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo01_Encapsulacion;

namespace Ejemplo02_Herencia
{
   public  class CocheElectrico : Coche, ICloneable
    {
        double nivelBateria;
        public CocheElectrico()
        {
            Marca = "";
            Modelo = "";
            Precio = 0;
            NivelBateria = 100;
        }

        public CocheElectrico(string marca, string modelo, float precio) : base(marca, modelo, precio)
        {
            NivelBateria = 100;
        }

        public CocheElectrico(string marca, string modelo, float precio, double nivelBateria)
        {
            Marca = marca;
            Modelo = modelo;
            Precio = precio;
            NivelBateria = nivelBateria;
        }

        public double NivelBateria
        {
            get
            {
                return nivelBateria;
            }
            set
            {
                nivelBateria = value;
            }
        }

        public override void Acelerar()
        {
            base.Acelerar();
            nivelBateria -= 0.1;
        }

        public object Clone()
        {
            CocheElectrico cel = new CocheElectrico(Marca, Modelo, precio, NivelBateria);
            return cel;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            }
            else
            {
                CocheElectrico objCoche = (CocheElectrico)obj;
                return this.Modelo == objCoche.Modelo
                    && this.Marca.Equals(objCoche.Marca)
                    && this.precio == objCoche.precio
                    && this.NivelBateria == NivelBateria;
            }
        }

        public override string ToString()
        {
            return "Coche electrico " + this.Marca + " - " + this.Modelo + " - " + this.NivelBateria + "%. ";
        }

        public override void PedirPorConsola()
        {
            base.PedirPorConsola();
            Utilidades.PedirNumero<double>("Nivel de bateria", out nivelBateria);
        }
    }
}
