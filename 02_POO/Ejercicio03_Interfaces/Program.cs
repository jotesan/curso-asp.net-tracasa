﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio01_Encapsulacion;
using Ejercicio02_Herencia;
using System;

namespace Ejercicio03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario user = new Usuario("Tony Stark", 45, 1.8f);
            Console.WriteLine("Nombre con apellido: " + user.GetNombre());
            user.SetNombre(user.Nombre);
            Console.WriteLine("Nombre sin apellido: " + user.GetNombre());
            Console.WriteLine("");

            INombrable[] empleadoMes = new INombrable[2];
            empleadoMes[0] = new Empleado("Matt", 50, 1.7f, 5000);
            empleadoMes[1] = new CocheElectrico("Mercedes", "Clase A", 37000, 90);
            ((Empleado)empleadoMes[0]).SuCoche = (Coche)empleadoMes[1];
            Console.WriteLine("Empleado del mes: ");
            foreach (INombrable inom in empleadoMes)
            {
                Console.WriteLine(inom.ToString());
            }
            Console.WriteLine("");

            CocheElectrico miElectaurus = new CocheElectrico("Electaurus", "Tesla", 12000);
            CocheElectrico homerElectaurus = (CocheElectrico)miElectaurus.Clone();
            homerElectaurus.Modelo = "Edison";
            Console.WriteLine("Coche de Homer Simpson: ");
            Console.WriteLine(homerElectaurus.ToString());
            Console.WriteLine("");
            Console.WriteLine("Mi coche: ");
            Console.WriteLine(miElectaurus.ToString());
            Console.WriteLine("");

            user.PedirPorConsola();
            user.MostrarEnConsola();
            Console.WriteLine("");
            Empleado empleadoDelMes = (Empleado)empleadoMes[0];
            empleadoDelMes.PedirPorConsola();
            empleadoDelMes.MostrarEnConsola();
            Console.WriteLine("");

            Coche miCoche = new Coche("Audi", "TT", 15000);
            miCoche.PedirPorConsola();
            miCoche.MostrarEnConsola();
            Console.WriteLine("");
            CocheElectrico cocheEmpleadoMes = (CocheElectrico)empleadoMes[1];
            cocheEmpleadoMes.PedirPorConsola();
            cocheEmpleadoMes.MostrarEnConsola();
        }
    }
}
