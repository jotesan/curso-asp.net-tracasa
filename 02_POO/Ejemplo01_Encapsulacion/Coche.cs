﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class Coche : Object, INombrable, IMostrable
    {
        float velocidad;
        string modelo;
        protected float precio;

        public Coche()
        {
            Marca = "";
            Modelo = "";
            Precio = 0;
        }

        public Coche(string marca, string modelo, float precio)
        {
            Marca = marca;
            Modelo = modelo;
            Precio = precio;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            } else
            {
                Coche objCoche = (Coche)obj;
                return this.Modelo == objCoche.Modelo
                    && this.Marca.Equals(objCoche.Marca)
                    && this.precio == objCoche.precio;
            }
        }

        public override string ToString()
        {
            return "Coche " + this.Marca + " - " + this.Modelo + ". ";
        }

        public float Velocidad
        {
            get
            {
                return velocidad;
            }
        }

        public string Modelo
        {
            get
            {
                return modelo;
            }
            set
            {
                this.modelo = value;
            }
        }
        public float Precio
        {
            set
            {
                if (value >= 0)
                {
                    precio = value;
                }
                else
                {
                    precio = 0;
                }
            }
        }

        public string Marca
        {
            get;
            set;
        }
        public string Nombre
        {
            get 
            {
                return GetNombre();
            } set 
            {
                SetNombre(value);
            }
        }

        public virtual void Acelerar()
        {
            velocidad++;
        }

        public string GetNombre()
        {
            return Marca + " - " + Modelo;
        }

        /// <summary>
        /// Establece tanto marca como modelo
        /// </summary>
        /// <param name="unNombre">Recibe "Marca - Modelo"</param>
        public void SetNombre(string unNombre)
        {
            if (!string.IsNullOrEmpty(unNombre))
            {
                string[] separados = unNombre.Split("-");
                Marca = separados[0].Trim();
                if (separados.Length > 1)
                {
                    Modelo = separados[1].Trim();
                }
            }
        }

        public virtual void MostrarEnConsola()
        {
            Console.WriteLine(ToString());
            Console.WriteLine("  ** Precio: " + precio + " ** ");
        }
        
        public virtual void PedirPorConsola()
        {
            Console.WriteLine("Introduce nuevos datos para el " + Utilidades.GetSplittedWords(GetType().Name) + ": ");
            string marca;
            Utilidades.PedirTexto("Marca", out marca);
            Marca = marca;
            Utilidades.PedirTexto("Modelo", out modelo);
            Utilidades.PedirNumero<float>("Precio", out precio);
        }
    }
}
