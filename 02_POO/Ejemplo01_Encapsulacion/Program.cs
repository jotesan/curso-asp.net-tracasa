﻿using System;

namespace Ejemplo01_Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche();
            //miCoche.velocidad = 10.5;
            miCoche.Acelerar();
            miCoche.Acelerar();
            //miCoche.Velocidad = 100;
            miCoche.Modelo = "Kia";
            miCoche.Precio = -5;
            Console.WriteLine("Velocidad: " + miCoche.Velocidad);
            Console.WriteLine("Modelo: " + miCoche.Modelo);
        }
    }
}
