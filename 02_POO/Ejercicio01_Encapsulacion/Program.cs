﻿using System;

namespace Ejercicio01_Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario user1 = new Usuario("Joseba", 28, 1.60f);
            Console.WriteLine("Nombre: " + user1.Nombre);
            Console.WriteLine("Edad: " + user1.Edad);
            Console.WriteLine("Altura: " + user1.Altura);
            Console.WriteLine("");

            Usuario user2 = new Usuario("", 0, 0.01f);
            Console.WriteLine("Nombre: " + user2.Nombre);
            Console.WriteLine("Edad: " + user2.Edad);
            Console.WriteLine("Altura: " + user2.Altura);
        }
    }
}
