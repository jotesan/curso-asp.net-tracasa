﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio01_Encapsulacion
{
    public class Usuario : INombrable, IMostrable
    {
        string nombre;
        int edad;
        float altura;

        public Usuario()
        {
        }

        public Usuario(string nombre, int edad, float altura)
        {
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                {
                    nombre = "SIN NOMBRE";
                } else
                {
                    nombre = value.Trim();
                }
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if (value <= 0)
                {
                    edad = 1;
                } else
                {
                    edad = value;
                }               
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if (value < 0.1f)
                {
                    altura = 0.1f;
                } else
                {
                    altura = value;
                }               
            }
        }

        public virtual void MostrarEnConsola()
        {
            Console.WriteLine(ToString());
        }

        public virtual void PedirPorConsola()
        {
            Console.WriteLine("Introduce nuevos datos para el " + Utilidades.GetSplittedWords(GetType().Name) + ": ");
            Utilidades.PedirTexto("Nombre", out nombre);
            Utilidades.PedirNumero<int>("Edad", out edad);
            Utilidades.PedirNumero<float>("Altura", out altura);
        }

        public string GetNombre()
        {
            return Nombre;
        }

        /// <summary>
        /// Establece nombre
        /// </summary>
        /// <param name="unNombre">Recibe "Nombre Apellido"</param>
        public void SetNombre(string unNombre)
        {
            if (!string.IsNullOrEmpty(unNombre))
            {
                string[] separados = unNombre.Split(" ");
                Nombre = separados[0].Trim();
            }
        }

        public override string ToString()
        {
            return "Nombre: " + nombre + "  Edad: " + edad + " años  Altura: " + altura + " metros";
        }
    }
}
