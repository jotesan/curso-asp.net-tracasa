﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public interface IMostrable
    {
        void MostrarEnConsola();

        void PedirPorConsola();
    }
}
