﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class Utilidades
    {
        public static string GetSplittedWords(string texto)
        {
            char[] letras = texto.ToCharArray();
            texto = letras[0].ToString();
            for (int i = 1; i < letras.Length; i++)
            {
                if (letras[i].ToString() == letras[i].ToString().ToUpper())
                {
                    texto += " " + letras[i];
                }
                else
                {
                    texto += letras[i];
                }
            }
            return texto;
        }

        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.Write(nombreDato + ": ");
            do
            {
                varDato = Console.ReadLine();
            } while (string.IsNullOrEmpty(varDato));

        }

        public static void PedirNumero<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.Write(nombreDato + ": ");
            bool esNumeroOk;
            do
            {
                string str = Console.ReadLine();
                if (typeof(Tipo) == typeof(int))
                {
                    int numero;
                    esNumeroOk = int.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                } else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                    esNumeroOk = float.TryParse(str, out numero);
                    varNum = (Tipo)(object) numero;
                } else if (typeof(Tipo) == typeof(double))
                {
                    double numero;
                    esNumeroOk = double.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                } else
                {
                    //esNumeroOk = true;
                    //varNum = default(Tipo);                   
                    //Console.Error.WriteLine("ERROR: Tipo de dato no válido");
                    throw new FormatException("ERROR: Tipo de dato no válido");
                }
                if (!esNumeroOk) Console.WriteLine("Pon un número por favor");
            }
            while (!esNumeroOk);
        }
    }
}
