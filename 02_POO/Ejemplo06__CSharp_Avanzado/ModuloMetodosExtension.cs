﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06__CSharp_Avanzado.MetodosExt
{
    public static class ModuloMetodosExtension
    {
        public static string FormatearNombre(this Fruta laFruta)
        {
            return "FRUTA " + laFruta.Nombre.ToUpper();
        }

        public static string FormatearNombre(this Fruta laFruta, string formato)
        {
            return formato + " " + laFruta.Nombre.ToUpper();
        }

        public static string ToUpperString(this object cualquierObjeto)
        {
            return cualquierObjeto.ToString().ToUpper();
        }
        public static string ToUpperString(this Array cualquierArray)
        {
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                result += "\n - " + elem.ToUpperString();
            }
            return result;
        }

        public static string ToStringFiltrado(this Array cualquierArray, string cadenaFiltro)
        {
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                if (elem.ToString().ToUpper().Contains(cadenaFiltro.ToUpper()))
                    result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
    }
}
