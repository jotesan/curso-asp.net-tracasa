﻿using Ejemplo06__CSharp_Avanzado.MetodosExt;
using System;

namespace Ejemplo06__CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta esLaPera = new Fruta("La pera repera", 150.01f);
            Fruta piña = new Fruta("Piña", 230.01f);
            Fruta platano = new Fruta("Plátano", 140.01f);
            Console.WriteLine(esLaPera.ToString());
            Console.WriteLine(esLaPera.FormatearNombre());
            Console.WriteLine(esLaPera.FormatearNombre("Texto formato"));
            Console.WriteLine(esLaPera.ToUpperString());
            int[] otroArray = new int[] {3, 2, 1};
            Console.WriteLine(otroArray.ToUpperString());
            string[] otroArrayStr = new string[] { "Tres", "Dos", "Uno" };
            Console.WriteLine(otroArrayStr.ToUpperString());
            Fruta[] frutas = new Fruta[] { esLaPera, piña, platano };
            Console.WriteLine(frutas.ToUpperString());
            Console.WriteLine(otroArrayStr.ToStringFiltrado("s"));
            Console.WriteLine(frutas.ToStringFiltrado("p"));
        }
    }
}
