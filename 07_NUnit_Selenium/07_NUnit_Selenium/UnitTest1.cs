using NUnit.Framework;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Reflection;
using OpenQA.Selenium.Support.UI;
using System;
using OpenQA.Selenium.Interactions;

namespace _07_NUnit_Selenium
{
    public class Tests
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            // string fichFirefox = "C:/Users/pmpcurso1/AppData/Local/Mozilla Firefox/firefox.exe";
            string fichFirefox = "../../../../FirefoxPortable/App/Firefox64/firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }

            if (File.Exists(fichFirefox))
            {
                // string rutaDriverFirefox = "geckodriver.exe";
                // FirefoxBinary binarioFirefox = new FirefoxBinary(fichFirefox);
                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";

                FirefoxOptions firefoxoptions = new FirefoxOptions();
                firefoxoptions.BrowserExecutableLocation = fichFirefox;
                firefoxoptions.AcceptInsecureCertificates = true;

                driver = new FirefoxDriver(geckoService, firefoxoptions);
            }
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            // driver.Close();
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            driver.Navigate().GoToUrl("https://www.duckduckgo.com/");
            IWebElement textoBusq = driver.FindElement(By.Name("q"));
            textoBusq.SendKeys("SQL Tutorials w3school");
            IWebElement botonBusq = driver.FindElement(By.Id("search_button_homepage"));
            botonBusq.Click();
            // var enlaces = driver.FindElements(By.CssSelector(".result__a"));
            var enlaces = driver.FindElements(By.CssSelector("a[href*='https://www.w3schools.com']"));
            foreach (var enlace in enlaces)
            {
                if (!enlace.Equals(enlaces[0]) && enlace.Displayed)
                {
                    // driver.Url = enlace.GetAttribute("href");
                    enlace.Click();
                    break;
                }
            }
            Assert.GreaterOrEqual(enlaces.Count, 3, "No se han encontrado enlaces");
            driver.FindElement(By.Id("accept-choices")).Click();

            Actions accion = new Actions(driver);
            var enlaceSQL = driver.FindElement(By.CssSelector("a[href='sql_datatypes.asp']"));
            accion.MoveToElement(enlaceSQL);
            // for (int i = 0; i < 20; i++) accion.KeyDown(Keys.ArrowDown);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", enlaceSQL);
            Wait(3);
            enlaceSQL.Click();

            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));
            Assert.IsNotNull(tituloNumDT, "No se han encontrado los tipos de datos num�ricos");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);
        }

        [Test]
        public void TestMostrarTablaEnConsola()
        {
            driver.Navigate().GoToUrl("https://www.w3schools.com/sql/sql_datatypes.asp");

            var theadCols = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table//tr/th"));
            var tableCol1 = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table//tr/td[1]"));
            var tableCol3 = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table//tr/td[3]"));

            Assert.AreEqual(tableCol1.Count, tableCol3.Count, "No cuadran las columnas de la tabla");
            Assert.GreaterOrEqual(tableCol1.Count, 5, "No son suficientes las columnas de la tabla");
            Assert.AreEqual(tableCol1[tableCol3.Count - 1].Text, "real", "La �ltima fila debe ser 'real'");
            Assert.AreEqual(theadCols.Count, 3, "No cuadran las columnas de la cabecera de la tabla");

            Console.WriteLine(theadCols[0].Text + "  " + theadCols[2].Text);

            for (int i = 0; i < tableCol1.Count; i++)
            {
                Console.WriteLine(tableCol1[i].Text + "  " + tableCol3[i].Text);
            }
        }

            public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeInic = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeInic) > delay);
        }
    }
}