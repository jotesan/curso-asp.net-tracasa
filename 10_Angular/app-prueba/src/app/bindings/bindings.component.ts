import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  template: 
  `<p>¡trozo-web funciona!</p>
  <p>Mostrando valor propiedad con interpolación (variable interpolada)</p>
  <p>{{ "" + (10 + 5) + propiedadClase + " - " + contadorComp }}</p>
  <input type="button" value="Aumentar" (click)="alPulsarBoton()"/>
  <p>Pulsado {{ contadorComp }} veces</p>
  <span>Cambia el valor: </span><input type="number" [(ngModel)]="contadorComp"/>`
})
export class BindingsComponent implements OnInit {

  propiedadClase: string;
  static contadorEstatico: number = 0;
  contadorComp = 0;

  constructor() { 
    this.propiedadClase = "...";
    this.contadorComp = 1;
  }

  ngOnInit(): void {
    BindingsComponent.contadorEstatico++;
    this.propiedadClase = `ngOnInit es el primer método del ciclo de vida del componente 
    (ejecutado ${BindingsComponent.contadorEstatico} veces)`
  }

  alPulsarBoton(): void {
    this.contadorComp++; 
  }
}