﻿using System;

namespace Ejercicio13_Estructuras
{
    public struct Usuario
    {
        public string nombre;
        public int edad;
        public float altura;

        public Usuario(string nombre, int edad, float altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.altura = altura;
        }

        public void MostrarUsuario()
        {
            Console.WriteLine("");
            Console.WriteLine("=====================================================");
            Console.WriteLine("Nombre: " + nombre + ".");
            Console.WriteLine("Edad:   " + edad.ToString() + " Años");
            Console.WriteLine("Altura: " + altura.ToString() + "m.");
            Console.WriteLine("=====================================================");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Usuario[] personas = new Usuario[4];

            personas[0] = new Usuario("Pijus Magnificus", 22, 175);
            personas[1] = new Usuario("Incontinencia Suma", 25, 164);

            for (int cont=2; cont < personas.Length; cont++)
            {
                Console.Write("Introduzca el nombre: ");
                string nombre = Console.ReadLine();

                Console.Write("Introduzca la edad: ");
                int edad = int.Parse(Console.ReadLine());

                Console.Write("Introduzca la altura (en M.): ");
                float altura = float.Parse(Console.ReadLine());
                personas[cont] = new Usuario(nombre, edad, altura);
            }

            for (int cont = 0; cont < personas.Length; cont++)
            {
                personas[cont].MostrarUsuario();
            }

        }

    }
    
}
