﻿using System;

namespace Ejemplo06_operadorInterrogantes
{
    class Program
    {
        static void Main(string[] args)
        {
            int? num1 = null;
            int num2 = num1 ?? 2;
            num1 ??= 1;
            Console.WriteLine(num1 + " y " + num2);
        }
    }
}
