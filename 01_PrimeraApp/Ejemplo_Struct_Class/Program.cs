﻿using System;

namespace Ejemplo_Struct_Class
{
    struct ProductoE
    {
        public string nombre;
        public float precio;

        public ProductoE(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void MostrarUsuario()
        {
            Console.Write("Nombre: ");
            Console.WriteLine(nombre);
            Console.Write("Precio: ");
            Console.WriteLine(precio);
        }
    }
    class ProductoC
    {
        public string nombre;
        public float precio;

        public ProductoC(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void MostrarUsuario()
        {
            Console.Write("Nombre: ");
            Console.WriteLine(nombre);
            Console.Write("Precio: ");
            Console.WriteLine(precio);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ProductoE productoE = new ProductoE("Melón",4.20f);
            ProductoC productoC = new ProductoC("Sandía", 5.40f);

            Console.WriteLine("ProductoE antes y después: ");
            Console.WriteLine("Nombre: " + productoE.nombre);
            Console.WriteLine("Precio: " + productoE.precio);
            Console.WriteLine("");
            cambiarProductoE(productoE);
            Console.WriteLine("ProductoE antes y después: ");
            Console.WriteLine("Nombre: " + productoE.nombre);
            Console.WriteLine("Precio: " + productoE.precio);
            Console.WriteLine("");
            cambiarProductoERef(ref productoE);
            Console.WriteLine("ProductoE antes y después: ");
            Console.WriteLine("Nombre: " + productoE.nombre);
            Console.WriteLine("Precio: " + productoE.precio);
            Console.WriteLine("\n");
            Console.WriteLine("ProductoC antes y después: ");
            Console.WriteLine("Nombre: " + productoC.nombre);
            Console.WriteLine("Precio: " + productoC.precio);
            Console.WriteLine("");
            cambiarProductoC(productoC);
            Console.WriteLine("ProductoC antes y después: ");
            Console.WriteLine("Nombre: " + productoC.nombre);
            Console.WriteLine("Precio: " + productoC.precio);
            Console.WriteLine("");
            cambiarProductoCRef(ref productoC);
            Console.WriteLine("ProductoC antes y después: ");
            Console.WriteLine("Nombre: " + productoC.nombre);
            Console.WriteLine("Precio: " + productoC.precio);
        }

        static void cambiarProductoE(ProductoE productoE)
        {
            productoE.nombre = "Plátano";
            productoE.precio = 1.25f;
        }
        static void cambiarProductoERef(ref ProductoE productoE)
        {
            productoE.nombre = "Manzana";
            productoE.precio = 2.99f;
        }
        static void cambiarProductoC(ProductoC productoC)
        {
            productoC.nombre = "Pera";
            productoC.precio = 1.75f;
        }
        static void cambiarProductoCRef(ref ProductoC productoC)
        {
            productoC.nombre = "Naranja";
            productoC.precio = 3.89f;
        }
    }
}
