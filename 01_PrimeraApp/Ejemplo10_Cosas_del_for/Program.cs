﻿using System;

namespace Ejemplo10_Cosas_del_for
{
    class Program
    {
        static void Main(string[] args)
        {
            int puntos, coordenadaX, coordenadaY, cuadrante1 = 0,cuadrante2 = 0, cuadrante3 = 0, cuadrante4 = 0;
            Console.WriteLine("¿Cuantos puntos? ");
            puntos = int.Parse(Console.ReadLine());
            for (int i = 0; i < puntos; i++)
            {
                Console.WriteLine("Coordenada X de punto " + (i + 1) + ": ");
                coordenadaX = int.Parse(Console.ReadLine());
                Console.WriteLine("Coordenada Y de punto " + (i + 1) + ": ");
                coordenadaY = int.Parse(Console.ReadLine());
                if (coordenadaX >= 0 && coordenadaY >= 0)
                {
                    cuadrante1++;
                } else if (coordenadaX < 0 && coordenadaY < 0)
                {
                    cuadrante3++;
                } else if (coordenadaX >= 0 && coordenadaY < 0)
                {
                    cuadrante2++;
                }
                else if (coordenadaX < 0 && coordenadaY >= 0)
                {
                    cuadrante4++;
                }
            }
            Console.WriteLine("Número de puntos en el cuadrante 1: " + cuadrante1);
            Console.WriteLine("Número de puntos en el cuadrante 2: " + cuadrante2);
            Console.WriteLine("Número de puntos en el cuadrante 3: " + cuadrante3);
            Console.WriteLine("Número de puntos en el cuadrante 4: " + cuadrante4);
        }
    }
}
