﻿using System;

namespace Ejemplo12_TratamientoCadenas
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto = "       En un lugar de La Mancha de cuyo nombre me quiero acordar, pero no me acuerdo.       ";
            Console.WriteLine("Original: " + texto);
            Console.WriteLine("Sin espacios: " + texto.Trim());
            Console.WriteLine("Mayus: " + texto.ToUpper());
            Console.WriteLine("Minus: " + texto.ToLower());
            Console.WriteLine("Un cacho: " + texto.Substring(20,20));
            Console.WriteLine("Un cacho hasta el final: " + texto.Substring(20));
            Console.WriteLine("La Mancha?: " + texto.IndexOf("La Mancha"));
            Console.WriteLine("LA MANCHA?: " + texto.ToUpper().IndexOf("LA MANCHA"));
            Console.WriteLine("Pamplona?: " + texto.IndexOf("Pamplona"));

            string[] palabras = texto.Split(" ");
            Console.WriteLine("palabra por palabra: ");
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);
            }
            Console.WriteLine("Por Pamplona:" + texto.Replace("La Mancha", "Pamplona").Replace("no", "Sarriguren"));
            
            Console.WriteLine("\n\n");
            palabras = new string[3];
            for (int i = 0; i < palabras.Length; i++)
            {
                Console.WriteLine("Palabra " + (i+1) + ": ");
                palabras[i] = Console.ReadLine().Trim();
            }
            string resultado = String.Join(",", palabras);
            resultado = String.Join(" ", resultado.Split(" ", StringSplitOptions.RemoveEmptyEntries));
            Console.WriteLine("Resultado: " + resultado);
        }
    }
}
