﻿using System;

namespace Ejemplo_Paso_Valor_Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;
            string cadena = "Hola mundo";
            Console.WriteLine("Entero dentro y antes: " + variableEnt);
            RecibimosUnValor(variableEnt);
            RecibimosUnaRef(ref variableEnt);
            Console.WriteLine("Entero dentro y después: " + variableEnt);
            Console.WriteLine("Texto original: " + cadena);
            Mayusculas(ref cadena);
            Console.WriteLine("Texto en mayúsculas: " + cadena);
        }
        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y después: " + entero);
        }
        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y después: " + entero);
        }
        static void Mayusculas(ref string texto)
        {
            texto = texto.ToUpper();
        }
    }
}
