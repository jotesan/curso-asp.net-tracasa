﻿CREATE TABLE [dbo].[Usuario] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Email]     VARCHAR (255) NOT NULL,
    [Nombre]    VARCHAR (50)  NOT NULL,
    [Edad]      TINYINT       NOT NULL,
    [Altura]    FLOAT (53)    NOT NULL,
    [Activo]    BIT           NOT NULL,
    [IdCoche]   INT           NULL,
    [IdTipoVia] INT           NULL,
    [NombreVia] VARCHAR (50)  NULL,
    [NumVia]    INT           NULL,
    [Piso]      VARCHAR (10)  NULL,
    [Puerta]    VARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Email] ASC),
    CONSTRAINT [FK_IdCoche_Coche] FOREIGN KEY ([IdCoche]) REFERENCES [dbo].[Coche] ([Id]),
    CONSTRAINT [FK_TipoVia] FOREIGN KEY ([IdTipoVia]) REFERENCES [dbo].[TipoVia] ([Id])
);

