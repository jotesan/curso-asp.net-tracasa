﻿DECLARE @Count AS INT
DECLARE @Usuario AS INT
DECLARE @Producto AS INT
DECLARE @FechaStr AS VARCHAR(100)
DECLARE @Fecha AS DATETIME
DECLARE @Cantidad AS INT

DELETE FROM CompraUsuarioProducto WHERE 1 = 1;

SET @Count = 0
WHILE @Count < 20
BEGIN
	SET @Count = @Count + 1
	SET @Usuario = 4 + RAND(CHECKSUM(NEWID())) * 10
	SET @Producto = 1 + RAND(CHECKSUM(NEWID())) * 5
	SET @Cantidad = 1 + RAND(CHECKSUM(NEWID())) * 100
	SET @FechaStr = CAST(ROUND(2018 + RAND(CHECKSUM(NEWID())) * 3, 0) AS VARCHAR)
			+ '-' + CAST(ROUND(1 + RAND(CHECKSUM(NEWID())) * 11, 0) AS VARCHAR)
			+ '-' + CAST(ROUND(1 + RAND(CHECKSUM(NEWID())) * 27, 0) AS VARCHAR);
	SET @Fecha = CAST(@FechaStr AS DATETIME)
	INSERT INTO CompraUsuarioProducto (IdUsuario, IdProducto, Fecha, Cantidad)
			VALUES (@Usuario, @Producto, @Fecha, @Cantidad)
	PRINT ' ID Usuario ' + CAST(@Usuario AS VARCHAR) + ' - ' + @FechaStr
END;
PRINT 'Final bucle';