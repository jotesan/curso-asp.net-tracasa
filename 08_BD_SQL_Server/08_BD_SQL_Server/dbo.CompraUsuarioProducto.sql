﻿CREATE TABLE [dbo].[CompraUsuarioProducto]
( 
    [IdUsuario] INT NOT NULL, 
    [IdProducto] INT NOT NULL, 
    [Fecha] DATETIME NOT NULL, 
	[Cantidad] TINYINT NOT NULL, 
    CONSTRAINT [PK_CompraUsuarioProducto] PRIMARY KEY ([IdUsuario], [IdProducto], [Fecha]), 
	FOREIGN KEY (IdUsuario) REFERENCES Usuario(Id),
	FOREIGN KEY (IdProducto) REFERENCES Producto(Id)
)
