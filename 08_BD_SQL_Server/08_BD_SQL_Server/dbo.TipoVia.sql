﻿CREATE TABLE [dbo].[TipoVia] (
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [Nombre] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Id_TipoVia] PRIMARY KEY CLUSTERED ([Id] ASC)
);

