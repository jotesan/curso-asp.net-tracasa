window.onload = function() {
    let btnAnadir = document.getElementById("btn-anadir");
    let usuario = JSON.parse(window.localStorage.getItem("mi-usuario"));
    console.log("Cargado: " + JSON.stringify(usuario));
    let filaTabla = `<tr><td>${usuario.nombre}</td><td>${usuario.edad}</td><td>${usuario.altura} m</td></tr>`;
    document.getElementById("tbody-usuarios").innerHTML = filaTabla;

    btnAnadir.onclick = function() {
        let aficiones = new Object();
        aficiones.leer = document.getElementById("aficiones-leer").checked;
        aficiones.musica = document.getElementById("aficiones-musica").checked;
        aficiones.cine = document.getElementById("aficiones-cine").checked;

        let usuario  = {
            "nombre": document.getElementById("nombre").value.toUpperCase(),
            "edad": parseInt(document.getElementById("edad").value),
            "altura": parseFloat(document.getElementById("altura").value),
            "aficiones" : aficiones
        };

        alert(textoAlertUsuario(usuario));
        window.localStorage.setItem("mi-usuario", JSON.stringify(usuario));
        window.location = window.location;
    };
    
    function textoAlertUsuario(usu) {
        return `Yepa ${usu.nombre} 
        Tienes ${usu.edad} y altura ${usu.altura} 
        te gusta ${usu.aficiones.leer ? " leer " : ""} 
        ${usu.aficiones.musica ? " musica " : ""} 
        ${usu.aficiones.cine ? " cine " : ""}!`
    };

    document.getElementById("btn-ir").addEventListener("click", function() {
        window.location = document.getElementById("url").value;
    });
};