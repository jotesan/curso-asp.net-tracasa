﻿using System;

namespace Ejemplo06_Funcion_Callback
{
    delegate float FuncionOperador(float op1, float op2);

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Invocamos VistaOperadora");
            //VistaCalculadora(Calculadora_A.SumarA);
            //VistaCalculadora(CalculadoraB.SumarB);
            VistaOperadora(CalculadoraB.SumarB, CalculadoraB.MultiplicarB);
            //VistaCalculadora(Calculadora_A.MultiplicarA);
            //VistaCalculadora(CalculadoraB.MultiplicarB);
        }

        static void VistaCalculadora(FuncionOperador operador)
        {
            Console.WriteLine("Operando 1:");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2:");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine("Resultado: " + resultado);
        }

        static void VistaOperadora(FuncionOperador funSumar, FuncionOperador funMul)
        {
            Console.WriteLine("Operación:");
            string op = Console.ReadLine();
            float x;
            float y;
            float aux;
            float resultado;
            string[] operandos;
            if (op.Contains("+"))
            {
                operandos = op.Split("+");
                if(!float.TryParse(operandos[0], out aux) || !float.TryParse(operandos[1], out aux))
                {
                    Console.WriteLine("No te entiendo, humano");
                    return;
                }
                x = float.Parse(operandos[0]);
                y = float.Parse(operandos[1]);
                resultado = funSumar(x, y);
            } else if (op.Contains("*"))
            {
                operandos = op.Split("*");
                if (!float.TryParse(operandos[0], out aux) || !float.TryParse(operandos[1], out aux))
                {
                    Console.WriteLine("No te entiendo, humano");
                    return;
                }
                x = float.Parse(operandos[0]);
                y = float.Parse(operandos[1]);
                resultado = funMul(x, y);
            }
            else
            {
                Console.WriteLine("No te entiendo, humano");
                return;
            }
            Console.WriteLine("Resultado: " + resultado);
        }
    }
}
