﻿using System;

namespace Wikipedia.Patterns.Strategy
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            Context context;

            // Tres contextos con diferentes estrategias
            context = new Context(new ConcreteStrategyA("Eeeeeeeeoh "));
            context.Execute();

            context = new Context(new ConcreteStrategyB());
            context.Execute();

            context = new Context(new ConcreteStrategyC("Nombre directo"));
            context.Execute();

        }
    }

    abstract class StrategyBase
    {
        protected string nombre;

        public StrategyBase(string mensaje)
        {
            nombre = mensaje + " " + GetType().Name + ".Execute()";
        }

        protected void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        public abstract void Execute();

        public virtual void MostrarNombre()
        {
            Console.WriteLine(nombre);
        }
    }

    // Implementa el algoritmo usando el patron estrategia
    class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje)
        {
        }

        public override void Execute()
        {
            RepetirChar('-', 35);
            MostrarNombre();
        }

        public override void MostrarNombre()
        {
            RepetirChar('-', 35);
            base.MostrarNombre();
            RepetirChar('-', 35);
        }
    }

    class ConcreteStrategyB : StrategyBase
    {
        public ConcreteStrategyB() : base("Llamar a")
        {
        }

        public override void Execute()
        {
            MostrarNombre();
            RepetirChar('\n', 3);
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("")
        {
            this.nombre = nombre;
        }

        public override void Execute()
        {
            RepetirChar('*', 20);
            Console.WriteLine("Called ConcreteStrategyC.Execute()");
            Console.WriteLine(this.nombre);
        }
    }

    // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
    class Context
    {
        StrategyBase strategy;

        // Constructor
        public Context(StrategyBase strategy)
        {
            this.strategy = strategy;
        }

        public void Execute()
        {
            strategy.Execute();
        }
    }
}