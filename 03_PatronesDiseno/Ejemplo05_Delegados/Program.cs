﻿using System;

namespace Ejemplo05_Lambdas
{
    delegate void FuncionQueRecibeInt(int param);
    
    class Program
    {
        static void Main(string[] args)
        {
            // LAMBDAS
            FuncionEstatica(5);
            OtraEstatica(7);

            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatica;
            funRecint(200);
            //funRecint = null;
            funRecint = OtraEstatica;
            funRecint(200);
            Console.WriteLine("\n\nOtra libreria, otro modulo, otra funcion, hace:");
            OtroSistema(OtraEstatica);
            OtroSistema(FuncionEstatica);
            OtroSistema(funRecint);
        }

        static void OtroSistema(FuncionQueRecibeInt funExt)
        {
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");
            funExt(3);
        }

        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada: ");
            Console.WriteLine("Param: " + x);
        }

        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra Estatica: ");
        }
    }
}
