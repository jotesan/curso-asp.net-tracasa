﻿using System;
using System.Collections.Generic;

namespace Ejemplo02_Factoria
{
    // https://refactoring.guru/es/design-patterns/factory-method/csharp/example

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vamos a la fabrica!");
            FactoriaProducto fabrica = new FactoriaProducto();
            //List<Producto> lista = new List<Producto>();
            //lista.Add(fabrica.Crear(1));
            //lista.Add(fabrica.Crear(2));
            //lista.Add(fabrica.Crear(3, "Tres de lo que sea"));
            //lista.Add(fabrica.Crear(4));
            List<Producto> lista = ClaseEstaticaFactoria.CrearLista(new int[] {2, 7, 3, 1});
            foreach (var item in lista)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }

        }
    }
}
