﻿using System;

namespace Ejemplo01_Singleton
{
    class Modulo2
    {
        static void Main(string[] args)
        {
            GestorTextos.GetInstancia.Nuevo("DDDD");
            GestorTextos.GetInstancia.Nuevo("EEEE");
            GestorTextos.GetInstancia.Nuevo("FFFF");
            GestorTextos.GetInstancia.Mostrar();
            Modulo1.Main2(null);
        }
    }
}
