﻿using System;
using OtroNameSpace;

namespace Ejercicio08_Strategy_Lambdas
{
    class Program
    {
        static void Main()
        {
            //Context context;

            //// Tres contextos con diferentes estrategias
            //context = new Context(new ConcreteStrategyA("Eeeeeeeeoh "));
            //context.Execute();

            //context = new Context(new ConcreteStrategyB());
            //context.Execute();

            //context = new Context(new ConcreteStrategyC("Nombre directo"));
            //context.Execute();

            StrategyObject estrategiaA = ConcretesStrategies.NewConcreteStrategyA("Eeeeeeeeeooh");
            estrategiaA.Execute();

            StrategyObject estrategiaB = ConcretesStrategies.NewConcreteStrategyB();
            estrategiaB.Execute();

            StrategyObject estrategiaC = ConcretesStrategies.NewConcreteStrategyC( "Nombre directo");
            estrategiaC.Execute();
        }
    }

    static class ConcretesStrategies
    {
        public static StrategyObject NewConcreteStrategyA(string mensaje)
        {
            StrategyObject strObj = new StrategyObject(mensaje, "ConcreteStrategyA");
            strObj.MostrarNombre = (nombre) =>
            {
                strObj.RepetirChar('_', 30);
                Console.WriteLine(nombre);
                strObj.RepetirChar('_', 30);
            };
            strObj.Execute = () =>
            {
                strObj.RepetirChar('-', 30);
                strObj.MostrarNombre(strObj.Nombre);
            };
            return strObj;
        }

        public static StrategyObject NewConcreteStrategyB()
        {
            StrategyObject strObj = new StrategyObject("Llamar a", "ConcreteStrategyB");
            strObj.MostrarNombre = (nombre) =>
            {
                Console.WriteLine(nombre);
            };
            strObj.Execute = () =>
            {
                strObj.MostrarNombre(strObj.Nombre);
                strObj.RepetirChar('\n', 3);
            };
            return strObj;
        }

        public static StrategyObject NewConcreteStrategyC(string nombreEspecifico)
        {
            StrategyObject strObj = new StrategyObject("Called", "ConcreteStrategyC");
            strObj.MostrarNombre = (texto) =>
            {
                Console.WriteLine(texto);
            };
            strObj.Execute = () =>
            {
                strObj.RepetirChar('*', 20);
                strObj.MostrarNombre(strObj.Nombre);
                strObj.MostrarNombre(nombreEspecifico);

            };
            return strObj;
        }

        static void Execute()
        {
            //RepetirChar('-', 35);
            //MostrarNombre();
        }
    }
    //abstract class StrategyBase
    //{
    //    protected string nombre;

    //    public StrategyBase(string mensaje)
    //    {
    //        nombre = mensaje + " " + GetType().Name + ".Execute()";
    //    }

    //    protected void RepetirChar(char caracter, int veces)
    //    {
    //        for (int i = 0; i < veces; i++)
    //        {
    //            Console.Write(caracter);
    //        }
    //        Console.Write("\n");
    //    }
    //    public abstract void Execute();

    //    public virtual void MostrarNombre()
    //    {
    //        Console.WriteLine(nombre);
    //    }
    //}

    // Implementa el algoritmo usando el patron estrategia
    //class ConcreteStrategyA : StrategyObj
    //{
    //    public ConcreteStrategyA(string mensaje) : base(mensaje)
    //    {
    //    }

    //    public override void Execute()
    //    {
    //        RepetirChar('-', 35);
    //        MostrarNombre();
    //    }

    //    public override void MostrarNombre()
    //    {
    //        RepetirChar('-', 35);
    //        base.MostrarNombre();
    //        RepetirChar('-', 35);
    //    }
    //}

    //class ConcreteStrategyB : StrategyObj
    //{
    //    public ConcreteStrategyB() : base("Llamar a")
    //    {
    //    }

    //    public override void Execute()
    //    {
    //        MostrarNombre();
    //        RepetirChar('\n', 3);
    //    }
    //}

    //class ConcreteStrategyC : StrategyObj
    //{
    //    public ConcreteStrategyC(string nombre) : base("")
    //    {
    //        this.nombre = nombre;
    //    }

    //    public override void Execute()
    //    {
    //        RepetirChar('*', 20);
    //        Console.WriteLine("Called ConcreteStrategyC.Execute()");
    //        Console.WriteLine(this.nombre);
    //    }
    //}

    // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
    //class Context
    //{
    //    StrategyBase strategy;

    //    // Constructor
    //    public Context(StrategyBase strategy)
    //    {
    //        this.strategy = strategy;
    //    }

    //    public void Execute()
    //    {
    //        strategy.Execute();
    //    }
    //}
}
