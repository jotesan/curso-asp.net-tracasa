﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtroNameSpace
{
    public delegate void ExecuteFunc();
    public delegate void MostrarNombreFunc(string nombre);
    public class StrategyObject
    {
        string nombre;
        
        public StrategyObject(string mensaje, string nombreEspecifico)
        {
            nombre = mensaje + " " + nombreEspecifico + ".Execute()";
        }

        public void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        ExecuteFunc execute;

        MostrarNombreFunc mostrarNombre;

        public string Nombre { get => nombre; }
        public ExecuteFunc Execute { get => execute; set => execute = value; }
        public MostrarNombreFunc MostrarNombre { get => mostrarNombre; set => mostrarNombre = value; }
    }

    //class Context
    //{
    //    StrategyBase strategy;

    //    // Constructor
    //    public Context(StrategyBase strategy)
    //    {
    //        this.strategy = strategy;
    //    }

    //    public void Execute()
    //    {
    //        strategy.Execute();
    //    }
    //}
}
