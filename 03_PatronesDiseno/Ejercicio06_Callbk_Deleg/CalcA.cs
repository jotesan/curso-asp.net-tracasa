﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Ejercicio06_Callbk_Deleg
{
    public static class CalcA
    {
        static public float Suma(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0) throw new Exception("El array no puede estar vacío");

            return numeros.Aggregate((x, y) => x + y);
        }

        static public float Resta(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0) throw new Exception("El array no puede estar vacío");

            return numeros.Aggregate((x, y) => x - y);
        }

        static public float Multiplicacion(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0) throw new Exception("El array no puede estar vacío");

            return numeros.Aggregate((x, y) => x * y);
        }

        static public float Division(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0) throw new Exception("El array no puede estar vacío");

            for (int i = 0; i < numeros.Length; i++)
            {
                if (i > 0 && numeros[i] == 0) throw new Exception("No se puede dividir por cero");
            }

            return numeros.Aggregate((x, y) => x / y);
        }
    }
}
