﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callbk_Deleg
{
    public static class CalcB
    {
        static public void Calcular(Func<float[], float> suma, Func<float[], float> resta, Func<float[], float> multiplicacion, Func<float[], float> division)
        {
            float num;
            int cantidad;
            List<float> numeros = new List<float>();
            string aux;
            string operacion = "";
            do
            {
                Console.Clear();
                Console.WriteLine("¿Cuántos números?");
                aux = Console.ReadLine();
            }
            while (!int.TryParse(aux, out cantidad) || int.Parse(aux) < 1);

            for (int i = 0; i < cantidad; i++)
            {
                do
                {
                    Console.Clear();
                    Console.WriteLine("Introduce número " + (i + 1) + "º: ");
                    aux = Console.ReadLine();
                }
                while (!float.TryParse(aux, out num));
                numeros.Add(num);
            }

            Dictionary<string, Func<float[], float>> operaciones = new Dictionary<string, Func<float[], float>>();
            operaciones.Add("+", suma);
            operaciones.Add("-", resta);
            operaciones.Add("*", multiplicacion);
            operaciones.Add("/", division);

            do
            {
                Console.Clear();
                Console.WriteLine("Introduce operador:");
                aux = Console.ReadLine();
            }
            while (!operaciones.ContainsKey(aux));

            Console.Clear();

            for (int i = 0; i < numeros.Count; i++)
            {
                if (i == numeros.Count - 1)
                {
                    operacion += numeros[i] + " = ";
                    break;
                }
                operacion += numeros[i] + " " + aux + " ";
            }
            Console.WriteLine("Operación: " + operacion + operaciones[aux](numeros.ToArray()));
        }

        static public float CalcularString(Func<float[], float> suma, Func<float[], float> resta, Func<float[], float> multiplicacion, Func<float[], float> division)
        {
            string operacion;
            string operador;
            float aux;
            List<float> numeros = new List<float>();
            Console.WriteLine("Introduce operación:");
            operacion = Console.ReadLine();
            operador = "+";
            if (operacion.IndexOf("+") < 0)
            {
                operador = "-";
                if (operacion.IndexOf("-") < 0)
                {
                    operador = "*";
                    if (operacion.IndexOf("*") < 0)
                    {
                        operador = "/";
                        if (operacion.IndexOf("/") < 0)
                        {
                            Console.WriteLine("Operador no encontrado");
                            return 0;
                        }

                    }
                }
            }
            if (float.TryParse(operacion.Replace(operador, ""), out aux))
            {
                string[] nums = (operacion.Split(operador));
                for (int i = 0; i < nums.Length; i++)
                {
                    numeros.Add(float.Parse(nums[i]));
                }
                switch (operador)
                {
                    case "+":
                        return suma(numeros.ToArray());
                    case "-":
                        return resta(numeros.ToArray());
                    case "*":
                        return multiplicacion(numeros.ToArray());
                    case "/":
                        return division(numeros.ToArray());
                    default:
                        Console.WriteLine("Operador incorrecto");
                        return 0;
                }
            }
            else
            {
                Console.WriteLine("Operación incorrecta");
                return 0;
            }
        }
    }
}
