﻿using System;

namespace Ejercicio06_Callbk_Deleg
{
    class Program
    {
        static void Main(string[] args)
        {
            CalcB.Calcular(CalcA.Suma, CalcA.Resta, CalcA.Multiplicacion, CalcA.Division);
            //Console.WriteLine("Resultado: " + CalcB.CalcularString(CalcA.Suma, CalcA.Resta, CalcA.Multiplicacion, CalcA.Division));
        }
    }
}
