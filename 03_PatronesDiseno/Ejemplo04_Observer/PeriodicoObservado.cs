﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not);
    class PeriodicoObservado
    {
        List<ISuscriptorObservador> listaSuscriptores;
        public NoticiaCiencia NuevaNotCiencia;

        public PeriodicoObservado()
        {
            listaSuscriptores = new List<ISuscriptorObservador>();
        }

        public void NoticiaCiencia(string laNoticia)
        {
            if (NuevaNotCiencia != null) NuevaNotCiencia("CIENCIA: " + laNoticia);
        }

        public void NotificarNoticia(string titular, DateTime fecha)
        {
            Console.WriteLine("¡Extra extra!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(fecha.ToString() + " -> " + titular + "\n");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticia(titular, fecha);
            }
        }

        public void NotificarNoticiaCorazon(string titular, DateTime fecha)
        {
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNoticiaCorazon(titular, fecha);
            }
        }

        public void AddSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Add(observador);
        }

        public void RemoveSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Remove(observador);
        }

        ~PeriodicoObservado()
        {
            NotificarNoticia("Este periodico se va a la quiebra", new DateTime(2021, 08, 21, 11, 00, 00));
            listaSuscriptores.Clear();
        }

        //public void Quiebra()
        //{
        //    NotificarNoticia("Este periodico se va a la quiebra", new DateTime(2021, 08, 21, 11, 00, 00));
        //    listaSuscriptores.Clear();
        //}
    }
}
