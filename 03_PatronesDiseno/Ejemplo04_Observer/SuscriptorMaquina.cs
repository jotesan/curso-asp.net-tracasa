﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorMaquina : ISuscriptorObservador
    {
        int codigo;

        public SuscriptorMaquina(int codigo)
        {
            this.codigo = codigo;
        }

        public void ActualizarNoticiaCorazon(string noticia, DateTime fecha)
        {
            Console.WriteLine("Pasando del tema...\n");
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Soy " + codigo + " -> Noticia " + noticia + "\n");
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine(fecha + "\n" + noticia + "\n");
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("../../../noticias_" + codigo + ".txt", fecha.ToString("R") + "\n" + noticia + "\n\n");
        }
    }
}
