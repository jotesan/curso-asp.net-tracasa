﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            MovidasObserver();
            System.GC.Collect();
            System.Threading.Thread.Sleep(1000);
        }

        static void MovidasObserver()
        {
            System.IO.File.Delete("../../../noticias_31415.txt");
            Console.WriteLine("Patron observer: Periodico\n");
            PeriodicoObservado alDia = new PeriodicoObservado();
            // 1 - Un suscriptor humano
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            // 2 - Ocurre una noticia
            alDia.NotificarNoticia("Un meteorito roza la Luna", DateTime.Now);
            // 3 - otro suscriptor humano
            SuscriptorHumano ana = new SuscriptorHumano("Ana");
            alDia.AddSuscriptor(ana);
            // 4 - otro suscriptor maquina, lleva un código int en vez de nombre
            SuscriptorMaquina pi = new SuscriptorMaquina(31415);
            alDia.AddSuscriptor(pi);
            // 5 - Ocurre otra noticia
            System.Threading.Thread.Sleep(2000);
            alDia.NotificarNoticia("El coronavirus es erradicado", DateTime.Now);
            alDia.NotificarNoticiaCorazon("Una actriz beso a un fan", DateTime.Now);
            // 6 - El otro humano se desuscribe porque dice mucha mentira
            alDia.RemoveSuscriptor(juan);
            System.Threading.Thread.Sleep(2500);
            // 7 - Ocurre la última noticia
            alDia.NotificarNoticia("Comienza la tercera guerra mundial", DateTime.Now);
            // 8 - El periodico cierra
            //alDia.Quiebra();
            // 9 - Que los suscriptores reciban la fecha y hora de la noticia y la muestren, claro

            alDia.NuevaNotCiencia = NationalGeo;
            alDia.NoticiaCiencia("El meteorito existia, y era de kriptonita!");
        }

        static void NationalGeo(string noticiaDePseudociencia)
        {
            Console.WriteLine(">>>>> A saber " + noticiaDePseudociencia);
        }
    }
}
