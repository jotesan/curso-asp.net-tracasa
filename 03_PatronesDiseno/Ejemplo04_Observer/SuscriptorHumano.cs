﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorHumano : ISuscriptorObservador
    {
        string nombre;

        public SuscriptorHumano(string nombre)
        {
            this.nombre = nombre;
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.WriteLine("¡Nuevas noticias, " + nombre + "!");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(fecha.ToString("f") + "\n" + noticia + "\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void ActualizarNoticiaCorazon(string noticia, DateTime fecha)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(noticia + "\n");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
