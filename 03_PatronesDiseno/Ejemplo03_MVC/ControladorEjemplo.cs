﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ControladorEjemplo
    {
        IModeloEjemplo modelo;

        public ControladorEjemplo(IModeloEjemplo modelo)
        {
            this.modelo = modelo;
        }

        public Ejemplo AltaEjemplo(int entero, string str)
        {
            return modelo.Crear(entero, str);
        }

        public IList<Ejemplo> MostrarEjemplos()
        {
            return modelo.LeerTodos();
        }

        public Ejemplo LeerUnoString(string nombre)
        {
            return modelo.LeerUnoString(nombre);
        }
        public Ejemplo LeerUnoInt(int numero)
        {
            return modelo.LeerUnoInt(numero);
        }

        public bool EliminarUnoString(string nombre)
        {
            return modelo.EliminarUnoString(nombre);
        }
        public bool EliminarUnoInt(int numero)
        {
            return modelo.EliminarUnoInt(numero);
        }

        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            return modelo.Modificar(nombreBusq, entero, str);
        }
    }
}
