﻿using System;

namespace Ejemplo03_MVC
{
    class Program
    {
        VistaEjemplo ve;

        Program()
        {
            IModeloEjemplo me = new ModeloEjemploLista();
            ControladorEjemplo ce = new ControladorEjemplo(me);
            ve = new VistaEjemplo(ce);
        }
        void ProbarDatos()
        {
            ve.AltaEjemplo();
            ve.AltaEjemplo();
            ve.AltaEjemplo();
            ve.MostrarEjemplos();
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            //program.ProbarDatos();
            program.ve.Menu();          
        }
    }
}
