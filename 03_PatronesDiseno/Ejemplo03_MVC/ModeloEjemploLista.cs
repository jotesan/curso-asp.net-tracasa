﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemploLista : IModeloEjemplo
    {
        private List<Ejemplo> ejemplos;

        public ModeloEjemploLista()
        {
            ejemplos = new List<Ejemplo>();
        }

        public Ejemplo Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
            return ejemplo;
        }
        //public List<Ejemplo> LeerTodos()
        //{
        //    return ejemplos;
        //}
        //public Ejemplo LeerUno(string str)
        //{
        //    foreach (Ejemplo ejemplo in ejemplos)
        //    {
        //        if (ejemplo.Str == str)
        //            return ejemplo;
        //    }
        //    return null;
        //}

        public Ejemplo Crear(int entero, string str)
        {
            return Crear (new Ejemplo(entero, str));
        }

        IList<Ejemplo> IModeloGenerico<Ejemplo>.LeerTodos()
        {
            return ejemplos;
        }

        public Ejemplo LeerUnoString(string nombre)
        {
            foreach (var item in ejemplos)
            {
                if (item.Str.ToUpper().Equals(nombre.ToUpper()))
                {
                    return item;
                }
            }
            return null;
        }
        public Ejemplo LeerUnoInt(int numero)
        {
            foreach (var item in ejemplos)
            {
                if (item.EsEntero(numero))
                {
                    return item;
                }
            }
            return null;
        }

        bool IModeloEjemplo.EliminarUnoString(string nombre)
        {
            Ejemplo ej = LeerUnoString(nombre);
            if (ej != null)
            {
                return ejemplos.Remove(ej);
            }
            return false;
        }

        bool IModeloGenerico<Ejemplo>.EliminarUnoInt(int numero)
        {
            Ejemplo ej = LeerUnoInt(numero);
            if (ej != null)
            {
                return ejemplos.Remove(ej);
            }
            return false;
        }

        Ejemplo IModeloEjemplo.Modificar(string nombreBusq, int entero, string str)
        {
            Ejemplo ejemploModif = LeerUnoString(nombreBusq);
            if (ejemploModif == null) return null;
            int posicion = ejemplos.IndexOf(ejemploModif);
            ejemplos[posicion] = new Ejemplo(entero, str);
            return ejemplos[posicion];
        }
    }
}
