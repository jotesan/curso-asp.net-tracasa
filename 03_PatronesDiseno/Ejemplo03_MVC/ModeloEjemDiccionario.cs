﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemDiccionario : IModeloEjemplo
    {
        Dictionary<string, Ejemplo> diccionario;

        public ModeloEjemDiccionario()
        {
            diccionario = new Dictionary<string, Ejemplo>();
        }

        public Ejemplo Crear(Ejemplo ejemplo)
        {
            throw new NotImplementedException("No se puede por la clave");
            //TODO: Leer propiedad privada mediante REFLECTION
        }

        public Ejemplo Crear(string key, int entero, string str)
        {
            Ejemplo ej = new Ejemplo(entero, str);
            if (diccionario.ContainsKey(key))
            {
                ej = null;
                //diccionario[clave] = ej;
            }
            else
            {
                diccionario.Add(key, ej);
            }
            return ej;
        }

        public Ejemplo Crear(int entero, string str)
        {
            string clave = str + "-" + entero;
            return Crear(clave, entero, str);
        }

        public void DevolverTodos(out Dictionary<string, Ejemplo> todos)
        {
            todos = diccionario;
        }

        public IList<Ejemplo> LeerTodos()
        {
            IList<Ejemplo> enumerable = new List<Ejemplo>();
            foreach (var item in diccionario)
            {
                enumerable.Add(item.Value);
            }
            return enumerable;
        }

        public Ejemplo LeerUnoString(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.ToUpper().Equals(nombre.ToUpper()))
                {
                    return item.Value;
                }
            }
            return null;
        }
        public Ejemplo LeerUnoInt(int numero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(numero))
                {
                    return item.Value;
                }
            }
            return null;
        }

        bool IModeloEjemplo.EliminarUnoString(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.ToUpper().Equals(nombre.ToUpper()))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;

        }

        bool IModeloGenerico<Ejemplo>.EliminarUnoInt(int numero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(numero))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }
        Ejemplo IModeloEjemplo.Modificar(string nombreBusq, int entero, string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.ToUpper().Equals(nombreBusq.ToUpper()))
                {
                    Ejemplo nuevoEjemploModificado = new Ejemplo(entero, str);
                    string clave = item.Key;
                    diccionario[clave] = nuevoEjemploModificado;
                    return nuevoEjemploModificado;
                }
            }
            return null;
        }
    }
}
