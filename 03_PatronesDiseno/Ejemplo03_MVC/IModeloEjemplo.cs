﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo : IModeloGenerico<Ejemplo>
    {
        public Ejemplo Crear(int entero, string str);
        //public IList<Ejemplo> LeerTodos();
        public Ejemplo LeerUnoString(string nombre);
        //public Ejemplo LeerUnoInt(int numero);
        public Ejemplo Modificar(string nombreBusq, int entero, string str);
        public bool EliminarUnoString(string nombre);
        //public bool EliminarUnoInt(int numero);
    }
}
