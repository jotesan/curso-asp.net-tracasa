﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class VistaEjemplo : IVistaEjemplo
    {
        ControladorEjemplo controlador;
        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
        }
        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("MENU: ( 0 -Salir)");
                Console.WriteLine("1 - Alta ejemplo");
                Console.WriteLine("2 - Mostrar todos");
                Console.WriteLine("3 - Mostrar uno");
                Console.WriteLine("4 - Eliminar uno");
                Console.WriteLine("5 - Modificar uno");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        case 4:
                            EliminarUno();
                            break;
                        case 5:
                            ModificarUno();
                            break;
                        default:
                            Console.WriteLine("Introduzca opción válida");
                            break;
                    }
                } else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }
        public void MostrarEjemplos()
        {
            IEnumerable<Ejemplo> todos = controlador.MostrarEjemplos();
            foreach (Ejemplo ejemplo in todos)
            {
                Console.WriteLine("Ejemplo " + ejemplo.ToString());
            }
        }
        public void MostrarUno()
        {
            Console.WriteLine(" 1 - Busqueda por número ");
            Console.WriteLine(" 2 - Busqueda por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Intro numero: ");
                        string numero = Console.ReadLine();
                        Ejemplo ejemploInt;
                        if (int.TryParse(numero, out int aux))
                        {
                            ejemploInt = controlador.LeerUnoInt(int.Parse(numero));
                            if (ejemploInt != null)
                                Console.WriteLine("Ejemplo " + ejemploInt.ToString());
                            else
                                Console.WriteLine("No encontrado por " + numero);
                        }
                        else
                        {
                            Console.WriteLine("número no valido");
                        }                                               
                        break;
                    case 2:
                        Console.WriteLine("Intro nombre: ");
                        string nombre = Console.ReadLine();
                        Ejemplo ejemploString = controlador.LeerUnoString(nombre);
                        if (ejemploString != null)
                            Console.WriteLine("Ejemplo " + ejemploString.ToString());
                        else
                            Console.WriteLine("No encontrado por " + nombre);
                        break;
                    default:
                        Console.WriteLine("Introduzca opción válida");
                        break;
                }
            }
            else
            {
                opcion = -1;
                Console.WriteLine("Escribe bien el número");
            }
        }
        public void EliminarUno()
        {
            Console.WriteLine(" 1 - Eliminar por número ");
            Console.WriteLine(" 2 - Eliminar por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Intro numero: ");
                        string numero = Console.ReadLine();
                        if (int.TryParse(numero, out int aux))
                        {
                            bool eliminadoNumero = controlador.EliminarUnoInt(int.Parse(numero));
                            if (eliminadoNumero)
                                Console.WriteLine("Eliminado " + numero);
                            else
                                Console.WriteLine("No encontrado por " + numero);
                        }
                        else
                        {
                            Console.WriteLine("número no valido");
                        }
                        break;
                    case 2:
                        Console.WriteLine("Intro nombre: ");
                        string nombre = Console.ReadLine();
                        bool eliminadoNombre = controlador.EliminarUnoString(nombre);
                        if (eliminadoNombre)
                            Console.WriteLine("Eliminado " + nombre);
                        else
                            Console.WriteLine("No encontrado por " + nombre);
                        break;
                    default:
                        Console.WriteLine("Introduzca opción válida");
                        break;
                }
            }
            else
            {
                opcion = -1;
                Console.WriteLine("Escribe bien el número");
            }
        }
        public void AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero:");
            //int entero = int.Parse(Console.ReadLine());
            string aux = Console.ReadLine();
            int entero;
            if (int.TryParse(aux, out entero))
            {
                entero = int.Parse(aux);
            }
            else
            {
                entero = 0;
            }
            Console.WriteLine("Alta ejemplo: string:");
            string str = Console.ReadLine();
            Ejemplo ej = this.controlador.AltaEjemplo(entero, str);
            if (ej != null)
            {
                Console.WriteLine("Se dio de alta el ejemplo " + ej.ToString());
            } else
            {
                Console.WriteLine("No se dio de alta el ejemplo " + str);
            }
        }

        public void ModificarUno()
        {
            Console.WriteLine("Intro nombre a buscar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerUnoString(nombre) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            } else
            {
                Console.WriteLine("Nuevo numero: ");
                string aux = Console.ReadLine();
                int entero;
                if (int.TryParse(aux, out entero))
                {
                    entero = int.Parse(aux);
                }
                else
                {
                    entero = 0;
                }
                Console.WriteLine("Nuevo string: ");
                string str = Console.ReadLine();
                Ejemplo ej = this.controlador.Modificar(nombre, entero, str);
                if (ej != null)
                {
                    Console.WriteLine("Modificado " + ej.ToString());
                }
                else
                {
                    Console.WriteLine("No encontrado por " + nombre);
                }
            }
        }
    }
}
