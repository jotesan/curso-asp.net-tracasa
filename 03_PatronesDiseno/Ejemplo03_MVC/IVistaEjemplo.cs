﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IVistaEjemplo
    {
        public void AltaEjemplo();
        public void MostrarUno();
        public void EliminarUno();
        public void ModificarUno();
        public void MostrarEjemplos();
        public void Menu();
    }
}
