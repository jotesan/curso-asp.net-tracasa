using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;

namespace _01_ProyectoTests
{
    public class Tests
    {
        string texto;

        [SetUp]
        public void Inicializacion()
        {
            texto = "Texto inicial";
        }

        [Test]
        public void TestPrimero()
        {
            Assert.Pass();
        }

        [Test]
        public void TestSegundo()
        {
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 3, 2 + 0);
            Assert.IsNotNull(this.texto, "Texto es nulo");
            texto = "Texto inicial 2";
        }

        public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado Cualquiera Ok");
        }

        public static void DelegadoCualquieraMal()
        {
            throw new NotImplementedException("Delegado Cualquiera Mal");
        }

        [Test]
        public void Test3()
        {
            Console.WriteLine("Antes del test 3");
            Assert.IsTrue(texto.Equals("Texto inicial"), "El texto no se ha incializado");
            Assert.Contains('i', texto.ToCharArray(), "El array de char texto no tiene 'i'");
            TestDelegate delegateOk = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk, "Delegado Cualquiera Ok FALL�");
            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado Cualquiera Ok CASC�");
            Console.WriteLine("Despu�s del test 3");
        }

        [Test(Author = "Joseba", Description = "Probando funciones para comprobar que no haya valores igual a cero o nulo")]
        public void Probando_NotZero_NotNull()
        {
            double pi = 3.1416;
            Assert.NotZero(pi, "El n�mero es cero");
            Assert.NotZero(10 / 3, "El resultado es cero");
            Assert.NotZero(100);
            // Solo falla cuando el valor o el resultado de la opoeraci�n pasados es 0
            // Assert.NotZero(24 % 2);
            // Assert.NotZero(0);
            object strObj = "Nuevo objeto";
            Assert.NotNull(strObj, "El objeto {0} es nulo", strObj.ToString());
            strObj = null;
            // Falla cuando el objeto pasado es null
            // Assert.NotNull(strObj, "El objeto strObject es nulo");
        }
    }
}